from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('register', views.register_view, name='register'),
    path('login', views.login_view, name='login'),
    path('logout', views.logout_view, name='logout'),
    path('contact', views.contact_view, name='contact'),
    path('profile/<user_id>', views.profile_view, name="profile"),
    path('search', views.contact_search, name='search'),

]