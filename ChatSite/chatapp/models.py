from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
# Create your models here.


class AccountManager(BaseUserManager):
    def create_user(self, username, email, firstname, lastname, password=None):

        if not email:
            raise ValueError("Users must have an email address.")
        if not username:
            raise ValueError("Users must have an username.")

        user = self.model(
            username=username,
            email=self.normalize_email(email),
            firstname=firstname,
            lastname=lastname,
        )

        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, firstname, lastname, email, password):
        user = self.create_user(
            username=username,
            email=self.normalize_email(email),
            firstname=firstname,
            lastname=lastname,
            password=password
        )

        user.is_staff = True
        user.is_superuser = True
        user.is_admin = True

        user.save()
        return user


class Account(AbstractBaseUser):
    username = models.CharField(max_length=60, unique=True)
    firstname = models.CharField(max_length=60, default="")
    lastname = models.CharField(max_length=60, default="")
    email = models.EmailField(max_length=60, unique=True)
    image = models.ImageField(default=None, upload_to='images/', blank=True, null=True)

    date_joined = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(auto_now=True)

    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)

    objects = AccountManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'firstname', 'lastname']

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True