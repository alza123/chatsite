import json

from django.conf import settings
from django.shortcuts import render, redirect

from django.http import HttpResponse, JsonResponse

from django.contrib.auth import login, authenticate, logout
from django.views.decorators.csrf import csrf_exempt

from ChatSite.chatapp.forms import RegistrationForm, AccountAuthenticationForm
from ChatSite.chatapp.models import Account


def index(request):
    if request.user.is_authenticated:
        return render(request, 'chatapp/index.html', context={'user': request.user.username})
    else:
        return redirect('login')


def profile_view(request, user_id):

    context = {}
    user_id = user_id
    try:
        account = Account.objects.get(pk=user_id) # PK?
    except Account.DoesNotExist:
        return HttpResponse("Not your own profile!")
    if account:
        context['id'] = account.id
        context['username'] = account.username
        context['firstname'] = account.firstname
        context['lastname'] = account.lastname
        context['email'] = account.email
        context['image'] = account.image

        is_self = True
        is_friend = False
        user = request.user
        if user.is_authenticated and user != account:
            is_self = False
        elif not user.is_authenticated:
            is_self = False
        context['is_self'] = is_self
        context['is_friend'] = is_friend
        context['BASE_URL'] = settings.BASE_URL

        return render(request, "chatapp/profile.html", context)


def contact_view(request):
    return render(request, 'chatapp/contact.html')


@csrf_exempt
def contact_search(request):
    if request.POST:
        username = request.POST['username']
        accounts = Account.objects.filter(username__contains=username).values()
        return JsonResponse(list(accounts), safe=False)


def login_view(request):

    context = {}

    user = request.user
    if user.is_authenticated:
        return redirect("index")

    if request.POST:
        form = AccountAuthenticationForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
            return redirect('index')
        else:
            context['login_form'] = form
    else:
        context['login_form'] = AccountAuthenticationForm()

    return render(request, "chatapp/login.html", context)


def logout_view(request):
    logout(request)
    return redirect("index")


def register_view(request):

    user = request.user

    if user.is_authenticated:
        return HttpResponse(f"You are already authenticated as {user.email}.")

    context = {}

    if request.POST:
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            account = authenticate(username=username, password=raw_password)
            login(request, account)
            return redirect("index")

        else:
            context['registration_form'] = form

    else:
        context['registration_form'] = RegistrationForm

    return render(request, 'chatapp/register.html', context)
